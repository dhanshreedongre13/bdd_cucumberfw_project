################# BDD Cucumber Framework Project ############

Cucumber is a testing tool based on Behavior Driven Development (BDD) framework. 
It is used to run functional tests written in plain text and develop test cases for software functionality.
It plays a supporting role in automated testing.


** Test Runner File ** :
Test Runner File is basically java file and it is kind of main method in
java in the cucumber project we have test runner file we will be define all attributes likes tages 
feature and these steps will be define what scenario we want to run the we peak the scenario and start the execution in a test runner file.

*** Feature File ***:
This is actually dot feature file it is a placed where will bw design all are screnario in the BDD cucumber scenario 
for the application in the feature file using the gherkin languages and also cucumber provides facility of 
adding the test data as well as in this feature file.

*** Step Defination File ***: 
Step defination file is java file when the cucumber like we will mention step by step like using editing languages Given When Then Method. 
 
**Test Script** :
In this we have all the available test scripts those needs to be created.

**Data/Variable File** :
We are using the excel files to input the data to our framework or requestBody parameters to input from excel files.

**API related common functions** :
 Which are used to trigget the API, extract the responseBody & extract the response status code.

**Utilities Common Methods** :
 We have class to create the directory, if it doesn't exist in the project for test log files and if it does exist than to delete the old one and create the new one for log files. Also we have another utility from which all the endpoint, requestBody, responseBody can be stored in the notepad/text file. And another utility to read the value/data from an excel file.

**Libraries** : 
We are using RestAssured, Apache-Poi,allure-testNG, testng libraries and dependency management of libraries are being managed by maven repository.

**Reports** : 
We are using Allure reports and Extent reports.








