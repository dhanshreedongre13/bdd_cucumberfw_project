package request_repository;

import java.io.IOException;

import java.util.ArrayList;
import Utility_common_methods.Excel_data_extracted;

public class Put_request_repository {

	public static String Put_request_tc1() throws IOException {
		ArrayList<String> Data = Excel_data_extracted.Excel_data_reader("Testdata", "Put_API", "Put_TC2");

		System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);

		String requestBody = "{\r\n" + "  " + "  \"name\": \"" + name + "\",\r\n" + "  " + "  \"job\": \"" + job
				+ "\"\r\n" + "}";

		return requestBody;
	}

}
