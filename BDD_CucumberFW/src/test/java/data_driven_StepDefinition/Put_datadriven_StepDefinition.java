package data_driven_StepDefinition;

import java.io.File;

import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_Put_Method;
import TestCase_package.Put_TC1;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_datadriven_StepDefinition {

	File log_dir;
	String endpoint;
	String requestBody;
	int StatusCode;
	String responseBody;

	@Given("Enter {string} and {string} in put request body")
	public void enter_and_in_put_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("Put_TC1_logs");
		endpoint = Put_endpoint.Put_endpoint_tc1();
		requestBody = "{\r\n" + "  " + "  \"name\": \"" + req_name + "\",\r\n" + "  " + "  \"job\": \"" + req_job
				+ "\"\r\n" + "}";

	}

	@When("Send the put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		StatusCode = Common_Put_Method.put_statusCode(requestBody, endpoint);
		responseBody = Common_Put_Method.put_responseBody(requestBody, endpoint);
		Handle_api_logs.evidence_creator(log_dir, "Put_TC1", endpoint, requestBody, responseBody);
		System.out.println(responseBody);

	}

	@Then("Validate put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(StatusCode, 200);

	}

	@Then("Validate put response body parameters")
	public void validate_put_response_body_parameters() throws IOException {
		Put_TC1.validator(requestBody, responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Put_TC1", endpoint, requestBody, responseBody);
		System.out.println("Put API response validation successful \n");

	}

}
