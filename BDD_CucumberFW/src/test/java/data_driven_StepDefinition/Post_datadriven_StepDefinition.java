package data_driven_StepDefinition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import TestCase_package.Post_TC1;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Post_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Post_datadriven_StepDefinition {
	File log_dir;
	String endpoint;
	String requestBody;
	int StatusCode;
	String responseBody;

	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		log_dir = Handle_directory.create_log_directory("Post_TC1_logs");
		endpoint = Post_endpoint.post_endpoint_tc1();
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
	}

	@When("Send the post request with payload")
	public void send_the_post_request_with_payload() {
		StatusCode = Common_method_handle_API.post_statusCode(requestBody, endpoint);
		responseBody = Common_method_handle_API.post_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
	}

	@Then("Validate post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(StatusCode, 201);
	}

	@Then("Validate post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		Post_TC1.validator(requestBody, responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Post_TC1", endpoint, requestBody, responseBody);
		System.out.println("Post API Response Body Validation Successfully \n");

	}

}
