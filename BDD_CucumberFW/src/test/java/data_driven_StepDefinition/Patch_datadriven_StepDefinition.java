package data_driven_StepDefinition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_Patch_Method;
import TestCase_package.Patch_TC1;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_datadriven_StepDefinition {
	File log_dir;
	String endpoint;
	String requestBody;
	int StatusCode;
	String responseBody;

	
	@Given("Enter {string} and {string} in patch request body")
	public void enter_and_in_patch_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("Put_TC1_logs");
		endpoint = Put_endpoint.Put_endpoint_tc1();
		requestBody = "{\r\n" + "    \"name\": \""+ req_name +"\",\r\n" + "    \"job\": \""+ req_job +"\"\r\n" + "}";
	}
	@When("Send the patch request with data")
	public void send_the_patch_request_with_data()  {
		StatusCode = Common_Patch_Method.patch_statusCode(requestBody, endpoint);
		responseBody = Common_Patch_Method.patch_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
	}
	@Then("Validate patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(StatusCode, 200);

	}
	@Then("Validate patch response body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		Patch_TC1.validator(requestBody, responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Patch_TC1", endpoint, requestBody, responseBody);
		System.out.println("Patch API response validation successful \n");
	}



}
