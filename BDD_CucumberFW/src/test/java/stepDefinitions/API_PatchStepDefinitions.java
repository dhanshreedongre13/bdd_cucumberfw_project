package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_common_methods.Common_Patch_Method;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Patch_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;

public class API_PatchStepDefinitions {

	static File log_dir;
	static int StatusCode;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@Given("Enter NAME and JOB in Patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		log_dir = Handle_directory.create_log_directory("Patch_TC1_logs");
		requestBody = Patch_request_repository.patch_request_tc1();
		endpoint = Patch_endpoint.patch_endpoint_tc1();
	}

	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() throws IOException {
		StatusCode = Common_Patch_Method.patch_statusCode(requestBody, endpoint);
		responseBody = Common_Patch_Method.patch_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Patch_TC1_logs", endpoint, requestBody, responseBody);

	}

	@Then("Validate patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(StatusCode, 200);

	}

	@Then("Validate patch response body parameters")
	public void validate_patch_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime Currentdate = LocalDateTime.now();
		String expecteddate = Currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date,expecteddate);
		System.out.println("Patch API Response Body Validation Successful" + "\n");
	}

}
