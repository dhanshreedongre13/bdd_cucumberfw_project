package stepDefinitions;

import java.io.File;


import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import API_common_methods.Common_Get_Method;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class API_GetStepDefinition {
	static File log_dir;
	static String endpoint;
	static String responseBody;
	static int StatusCode;
	
	
	
	@Given("Valide Get request Base URL")
	public void valide_get_request_base_url() {

		log_dir = Handle_directory.create_log_directory("Get_TC1_logs");
		endpoint = Get_endpoint.get_endpoint_tc1();
	}

	@When("Send the Get request with payload")
	public void send_the_get_request_with_payload() throws IOException {
		StatusCode = Common_Get_Method.get_statusCode(endpoint);
		responseBody = Common_Get_Method.get_responseBody(endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Get_TC1", endpoint, null, responseBody);

	}

	@Then("Validate Get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(StatusCode, 200);
	}

	@Then("Validate Get response body parameters")
	public void validate_get_response_body_parameters() {

		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);

		int id[] = { 1, 2, 3, 4, 5, 6 };
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String first_name[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String last_name[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		int count = dataarray.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_first_name = first_name[i];
			String exp_last_name = last_name[i];

			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_email = dataarray.getJSONObject(i).getString("email");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
			

		}
		System.out.println("Get API Response Body Validation Successful" + "\n");

			
		}
		
	}


