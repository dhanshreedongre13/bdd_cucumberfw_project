package stepDefinitions;

import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import API_common_methods.Common_Put_Method;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;

public class API_PutStepDefinition {
	static File log_dir;
	static int StatusCode;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	
	

	@Given("Enter NAME and JOB in Put request body")
	public void enter_name_and_job_in_put_request_body() throws IOException {
		log_dir = Handle_directory.create_log_directory("Put_TC1_logs");
		requestBody = Put_request_repository.Put_request_tc1();
		endpoint = Put_endpoint.Put_endpoint_tc1();
	}

	@When("Send the Put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		StatusCode = Common_Put_Method.put_statusCode(requestBody, endpoint);
		responseBody = Common_Put_Method.put_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir, "Put_TC1", endpoint, requestBody, responseBody);

	}

	@Then("Validate Put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(StatusCode, 200);

	}

	@Then("Validate Put response body parameters")
	public void validate_put_response_body_parameters() {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime Currentdate = LocalDateTime.now();
		String updatedAt = Currentdate.toString().substring(0, 11);
		
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, updatedAt);

		System.out.println("Put API Response Body Validation Successful" + "\n");
	}
	
}
