package Utility_common_methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extracted {

	public static ArrayList<String> Excel_data_reader(String filename, String sheetname, String TC_name)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");

		// step 1 : create the object of file input stream to locate the data file
		FileInputStream FIS = new FileInputStream(project_dir + "\\DataFiles\\" + filename + ".xlsx");

		// step 2 : create the XSSFWorkbook object to open the excel file
		XSSFWorkbook WB = new XSSFWorkbook(FIS);

		// step 3 : Fetch the number of sheets available in the excel file
		int count = WB.getNumberOfSheets();
		// System.out.println(count);

		// step 4 : acces the sheets as per the input sheet name

		for (int i = 0; i < count; i++) {
			String Sheetname = WB.getSheetName(i);
			// System.out.println(sheetname);

			if (Sheetname.equals(sheetname)) {
				System.out.println(sheetname);
				XSSFSheet sheet = WB.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String tc_name = datarow.getCell(0).getStringCellValue();
					if (tc_name.equals(TC_name)) {
						// System.out.println(TC_name);

						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							Arraydata.add(testdata);
						}

					}
				}
				break;

			}

		}
		WB.close();
		return Arraydata;

	}
}
