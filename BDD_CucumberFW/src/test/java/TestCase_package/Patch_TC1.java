package TestCase_package;

import java.io.File;


import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import API_common_methods.Common_Patch_Method;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Patch_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;

public class Patch_TC1 extends Common_Patch_Method {
	static int statusCode;
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("Patch_TC1_logs");
		requestBody = Patch_request_repository.patch_request_tc1();
		endpoint = Patch_endpoint.patch_endpoint_tc1();
	}

	
	public static void Patch_executor() throws IOException {

		for (int i = 0; i < 5; i++) {

			 statusCode = patch_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {
				String responseBody = patch_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Patch_TC1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime Currentdate = LocalDateTime.now();
		String expecteddate = Currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, expecteddate);

	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = Patch_TC1.class.getName();
		Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, requestBody, responseBody);

	}
}
