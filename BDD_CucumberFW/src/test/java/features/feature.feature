Feature: Trigger  API 
Scenario: Trigger the Post API request with valid request parameters 
	Given Enter NAME and JOB in request body 
	When Send the  request with payload 
	Then Validate status code 
	And Validate response body parameters 
	
Scenario: Trigger the Patch API request with valid request parameters 
	Given Enter NAME and JOB in Patch request body 
	When Send the Patch request with payload 
	Then Validate patch status code 
	And Validate patch response body parameters 
	

Scenario: Trigger the Put API request with valid request parameters 
	Given Enter NAME and JOB in Put request body 
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put response body parameters 

	Scenario: Trigger the Get API request with valid request parameters 
	Given Valide Get request Base URL
	When Send the Get request with payload 
	Then Validate Get status code 
	And Validate Get response body parameters 