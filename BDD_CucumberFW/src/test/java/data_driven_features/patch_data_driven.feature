Feature: Trigger Patch API 
@PatchAPI_TestCase
Scenario Outline: Trigger the Patch API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in patch request body 
	When Send the patch request with data
	Then Validate patch status code 
	And Validate patch response body parameters 
	
Examples:
			|Name |Job |
			|Dhanshree|QA|
			|Pankaj|Mgr|
			|Daksh|SrQA|	
	