package Cucumber.Options;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features="src/test/java/data_driven_features", glue= { "data_driven_StepDefinition" }, tags = "@PatchAPI_TestCase")
public class Data_Driven_TestRunner {

}    
