Feature: Trigger Put  API 
@PutAPI_TestCase
Scenario Outline: Trigger the Put API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in put request body 
	When Send the put request with payload 
	Then Validate put status code 
	And Validate put response body parameters 
	
Examples:
			|Name |Job |
			|Anu|QA|
			|Pankaj|Mgr|
			|Rekha|SrQA|	
	