Feature: Trigger Post  API
@PostAPI_TestCase 
Scenario Outline: Trigger the Post API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in post request body 
	When Send the post request with payload 
	Then Validate post status code 
	And Validate post response body parameters 
	
Examples:
			|Name |Job |
			|Dhanshree|QA|
			|Amol|Mgr|
			|Daksh|SrQA|	
	